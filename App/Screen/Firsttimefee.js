import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, SafeAreaView, TouchableOpacity, Picker, TextInput, Button } from 'react-native';
// import Icon from './node_modules/react-native-vector-icons/FontAwesome';
// import Icon1 from './node_modules/react-native-vector-icons/Entypo';
import { FontAwesome } from '@expo/vector-icons';


export default class Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            remainingfee: "",
            basicamt: "",
            indutraiamt: "",
            ojtamt: "",
            addon1amt: "",
            addon2amt: "",
            PayingFee: '',
            remainingfee: '',
            totalfee: '',
            Sample: [],
            data:[],
            language1: ''

        }
    }

    // componentDidUpdate() {this.loaddata()}

    InsertDataToServer = () => {
        const formData = new FormData
        formData.append('branch_id', this.props.navigation.state.params.branch_no);
        formData.append('cource_id', this.props.navigation.state.params.coursename);
        formData.append('batch_id', "14");
        formData.append('participat_id', this.state.Sample);
        formData.append('basic_amt', this.state.basicamt);
        formData.append('indutrai_amt', this.state.indutraiamt);
        formData.append('ojt_amt', this.state.ojtamt);
        formData.append('addon1_amt', this.state.addon1amt);
        formData.append('addon2_amt', this.state.addon2amt);
        //Add Api Here
        console.log("First", formData)
        fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/addfeeviewer', {
            method: 'POST',
            body: formData,

        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson.status)
                console.log("response", responseJson)
                if (responseJson.status === true) {
                    this.props.navigation.navigate('Listing')
                } else {
                    alert("Something Went Wrong ,Please try agin.")
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    api() {
        fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/Student/detail', { method: 'GET' })
            .then(response => response.json())
            .then(responseJson => {
                //console.log(responseJson.party_id, "oldest up")
                responseJson = responseJson.map(item => {
                    this.setState({ Sample: item.party_id })
                    return item;
                });
                console.log("fee================>>>>>>>>>>>>>>>>>>>>>>>>", this.state.Sample);

            })

    }
    option() {
        fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/option', {
            method: 'GET'
        })
        .then((response1) => response1.json())
        .then((response1Json) => {
            console.log(response1Json.courses);
            this.setState({
                data: response1Json.courses
            })
        })
    }
    componentDidMount() {
        this.api();
        this.option();
        const { navigation } = this.props;
        this.focusListener = navigation.addListener("didFocus", () => {
            this.setState({
                basicamt: "",
                indutraiamt: "",
                ojtamt: "",
                addon1amt: "",
                addon2amt: "",
            })
        })
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <SafeAreaView style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'stretch', backgroundColor: '#0186C2',
            }} >


                <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
                    <TouchableOpacity onPress={() => navigate('Home')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
                        <FontAwesome name="angle-left" size={30} color="#ffffff" />
                    </TouchableOpacity>
                    <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
                        <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>First Fee</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => console.log("save")}>
                        <FontAwesome name="edit" size={25} color="#ffffff" />
                    </TouchableOpacity>
                </View>


                <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>
                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>participat ID </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>{this.state.Sample}</Text>
                                </View>
                            </View>
                        </View>
                    </View>


                    <View style={styles.listingtouchgrey}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>Full Name</Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmainttext}>{this.props.navigation.state.params.name}</Text>
                                </View>
                            </View>
                        </View>
                    </View>


                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>Course Name </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    {/* <Text style={styles.listingmainttext}>{this.props.navigation.state.params.coursename}</Text> */}
                                    <TouchableOpacity
                                    style={{width:'100%'}}
                                     disabled={true}>
                                    <Picker style={{
                                        justifyContent: 'center', alignItems: 'center', height: 40,
                                        borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                                        width: "100%", fontWeight: 'bold', paddingLeft: 5
                                    }}
                                        selectedValue={this.props.navigation.state.params.coursename}
                                        onValueChange={(itemValue, itemPosition) =>
                                            this.setState({ language: itemValue, choosenIndex: itemPosition, })} >
                                               
                                        {this.state.data.map((item, key) => (
                                            <Picker.Item label={item.course_name} value={item.course_id} key={key} />)
                                        )}
                                    
                                    </Picker>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>Basic Amout </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                                        onChangeText={(basicamt) => this.setState({ basicamt })}
                                        value={this.state.basicamt}
                                        placeholder="Basic Amout" />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>Indutrial Traning </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                                        onChangeText={(indutraiamt) => this.setState({ indutraiamt })}
                                        value={this.state.indutraiamt}
                                        placeholder="Indutrial Traning" />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>OJT Amout </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                                        onChangeText={(ojtamt) => this.setState({ ojtamt })}
                                        value={this.state.ojtamt}
                                        placeholder="OJT Amout" />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>Add-on 1 </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                                        onChangeText={(addon1amt) => this.setState({ addon1amt })}
                                        value={this.state.addon1amt}
                                        placeholder="Add-on 1" />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listingtouch}>
                        <View style={styles.mainviewdetails}>

                            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                                    <Text style={styles.listingmaintitledetails}>Add-on 2 </Text>
                                </View>
                                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                                        onChangeText={(addon2amt) => this.setState({ addon2amt })}
                                        value={this.state.addon2amt}
                                        placeholder="Add-on 2" />
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <Button title="Save Information" onPress={this.InsertDataToServer} color="#2196F3" />


            </SafeAreaView >


        );
    }
}


const styles = StyleSheet.create({

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
        borderColor: '#000',
        borderBottomWidth: 2,
        backgroundColor: '#999',
        marginBottom: 10,
    },

    boxnumber: {
        color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
    },

    boxtitle: {
        color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
    },

    boxviewall: {
        color: "#ffffff", fontWeight: "500", fontSize: 12,
    },
    container: {
        paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
    },

    flexboxcode: {
        flex: 1, flexDirection: 'row',
    },

    box1: {
        width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
    },

    box2: {
        width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
    },

    box3: {
        width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
    },

    box4: {
        width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
    },

    box5: {
        width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
    },

    box6: {
        width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
    },
    circlered: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2, marginRight: 10,
        backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
    },
    circlegreen: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2, marginRight: 10,
        backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
    },
    circleyellow: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2, marginRight: 10,
        backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
    },
    listingtouch: {
        backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
    },
    circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
    listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
    approvestatus: { color: '#00a65a', fontSize: 13 },
    listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
    mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
    textviewwidth: { width: '75%' },
    titlespacing: { marginBottom: 5 },


    mainviewdetails: { padding: 20, flexDirection: 'row', height: 60 },

    listingmainpriority: { color: 'green', fontWeight: '500', fontSize: 16 },

    listingtouchgrey: {
        backgroundColor: '#f9f9f9', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
    },

    listingmaintitledetails: { color: 'black', fontWeight: '500', fontSize: 15 },
    listingmainttext: { color: 'black', fontWeight: '400', fontSize: 15 },

})




