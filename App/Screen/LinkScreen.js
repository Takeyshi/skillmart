import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BackButton from '../Component/BackButton';

import { LinearGradient } from 'expo-linear-gradient';

export default class LinkScreen extends Component {
  render() {
    return (
      <LinearGradient colors={['#018ec5','#155B7D','#124C69']} style={{ flex:1 }}>
        <BackButton navigation={this.props.navigation}/>
        <Text>Hello, LinkScreen!</Text>
      </LinearGradient>
    );
  }
}
