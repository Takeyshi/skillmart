import React from 'react';
import { StyleSheet, View, TouchableOpacity,Text } from 'react-native';
import PDFReader from 'rn-pdf-reader-js';
import BackButton from '../Component/BackButton';
import { FontAwesome } from '@expo/vector-icons';

export default class App extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        {/* <BackButton navigation={this.props.navigation}/> */}
        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={30} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Invoice PDF</Text>
          </View>
          </View>
        <PDFReader
          source={{ uri: "http://www.stagingenv.co.nz/apsystem/invoice.pdf" }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});