import React from 'react';
import { StyleSheet,View } from 'react-native';

import {Entypo} from "@expo/vector-icons";


export default class MenuButton extends React.Component{
    render(){
        return(
            
                      
          <View style={{height:'10%' ,flexDirection: 'row',backgroundColor:'#0186C2'}}>
          <View style={{width: '20%',  }} >
          <Entypo
            name="chevron-left"
            color="white"
            size={32}
            style={styles.menuIcon}
            onPress={() => this.props.navigation.goBack()}
          />
          </View>
          <View style={{width: '60%', }} />        
           <View style={{width: '20%', }} />
        </View>
        )
    }
}

const styles = StyleSheet.create({
    menuIcon:{
        zIndex:9,
        position:'absolute',
        top:30,
        left:20,
    },
   
})