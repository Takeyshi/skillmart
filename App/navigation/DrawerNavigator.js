import React from 'react';
import { platform, Dimensions } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';






import MenuDrawer from '../Component/MenuDrawer';
//Admin

import HomeScreen from '../Screen/HomeScreen';
import Login from '../Login';
import Firsttimefee from '../Screen/Firsttimefee';
import Attendancelist from '../Screen/attendancelist';
import Feelist from '../Screen/feelist';
import Selections from '../Screen/selections';
import Selectionsfee from '../Screen/selectionsfee';
import Feedetails from '../Screen/feedetails';
import Editfeesdetails from '../Screen/edit-feesdetails';
import Examlist from '../Screen/examlist';Examstudentlist
import Examstudentlist from '../Screen/examstudentlist';
import Selectionsexam from '../Screen/selectionsexam';
import Examdetails from '../Screen/examdetails';
import Editexamdetails from '../Screen/edit-examsdetails';

//Admine Invoice
import Listing from '../Screen/listing';
import Details from '../Screen/details';
import Editdetails from '../Screen/edit-details';
import pdfview from '../Screen/pdfview';
import Approverpdf from '../Screen/Approverpdf';

//Manage User
import Manageuser from '../Screen/manage-user';
import Userdetails from '../Screen/user-details';
import Adddetails from '../Screen/add-details';

//Manage WorkFlow
import Workflowlisting from '../Screen/workflow-listing';
import Addworkflow from '../Screen/add-workflow';

//Approve
import Approverdashboard from '../Screen/approver-dashboard';
import Approverdetails from '../Screen/approver-details';
import Approverlisting from '../Screen/approver-listing';
import Editapproverdetails from '../Screen/edit-approver-details';

import Approverinvoicelisting from '../Screen/approver-invoice-listing';
import Approverinvoicedetails from '../Screen/approver-invoice-details';

//Verifier
import Verifierdashboard from '../Screen/verifier-dashboard';
import Verifierlisting from '../Screen/verifier-listing';
import Verifierdetails from '../Screen/verifier-details';
import Editverifierdetails from '../Screen/edit-verifier-details';
//here
import Verifierinvoicelisting from '../Screen/verifier-invoice-listing';
import Verifierinvoicedetails from '../Screen/verifier-invoice-details';

import Extracteddata from '../Screen/extracted-data';
import Extracteddataverifier from '../Screen/extracted-data-verifier';





const WIDTH = Dimensions.get('window').width;


const DrawerConfig = {
    drawerwidth: WIDTH * 0.83,

    contentComponent: ({ navigation }) => {
        return (<MenuDrawer navigation={navigation} />)
    }
}

const DrawerNavigator = createDrawerNavigator(
    {
        Login: {
            screen: Login,
        },      
        Home: {
            screen: HomeScreen,
        },
        Listing: {
            screen: Listing,
            gesturesEnabled: false,
        },
        Attendancelist: {
            screen: Attendancelist,
        },
        Feelist: {
            screen: Feelist,
        },
        Feedetails: {
            screen: Feedetails,
        },
        Editfeesdetails: {
            screen: Editfeesdetails,
        },
        Selections: {
            screen: Selections,
        },
        Selectionsfee: {
            screen: Selectionsfee,
        },
        Examlist: {
            screen: Examlist,
        },
        Examstudentlist: {
            screen: Examstudentlist,
        },
        Examdetails: {
            screen: Examdetails,
        },
        Editexamdetails: {
            screen: Editexamdetails,
        },
        Selectionsexam: {
            screen: Selectionsexam,
        },

        Manageuser: {
            screen: Manageuser,
        },
        Workflowlisting: {
            screen: Workflowlisting,
        },
        Details: {
            screen: Details,
        },
        Editdetails: {
            screen: Editdetails,
        },
        Approverdashboard: {
            screen: Approverdashboard,
        },

        Approverinvoicelisting: {
            screen: Approverinvoicelisting,
        },
        Approverinvoicedetails: {
            screen: Approverinvoicedetails,
        },

        Approverdetails: {
            screen: Approverdetails,
        },
        Approverlisting: {
            screen: Approverlisting,
        },
        Editapproverdetails: {
            screen: Editapproverdetails,
        },
        Verifierdashboard: {
            screen: Verifierdashboard,
        },
        Verifierlisting: {
            screen: Verifierlisting,
        },
        Verifierdetails: {
            screen: Verifierdetails,
        },
        Verifierinvoicelisting: {
            screen: Verifierinvoicelisting,
        },
        Verifierinvoicedetails: {
            screen: Verifierinvoicedetails,
        },
        Editverifierdetails: {
            screen: Editverifierdetails,
        },
        Userdetails: {
            screen: Userdetails,
        },
        Adddetails: {
            screen: Adddetails,
        },
        Addworkflow: {
            screen: Addworkflow,
        },
        pdfview: {
            screen: pdfview,
        },
        Approverpdf: {
            screen: Approverpdf,
        },

        Extracteddata: {
            screen: Extracteddata,
        },

        Extracteddataverifier: {
            screen: Extracteddataverifier,
        },

        Firsttimefee:{
            screen:Firsttimefee,
        }

    },
        DrawerConfig
)

export default createAppContainer(DrawerNavigator);