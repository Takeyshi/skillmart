import React, { Component } from 'react';
import { Text, View, ScrollView,  StyleSheet, TouchableOpacity, SafeAreaView, Platform, Alert, BackHandler  } from 'react-native';

import { FontAwesome } from '@expo/vector-icons';

export default class dashboard extends Component {

 

  render() {
    const { navigate } = this.props.navigation;  
    return (
 
<SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>

        
          <ScrollView style={{backgroundColor: '#ffffff'}}>

            <View>
            
              
              <View style={styles.container}>
            
              <View style={styles.flexboxcode}>

                <TouchableOpacity onPress={() =>navigate('Listing')} style={styles.box1}>
                  <Text style={styles.boxnumber}>150</Text>
                  <Text style={styles.boxtitle}>Student Registration</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() =>navigate('Listing')} style={styles.box2}>
                  <Text style={styles.boxnumber}>53</Text>
                  <Text style={styles.boxtitle}>Fees</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>
              </View>

              <View style={styles.flexboxcode}>
                <TouchableOpacity onPress={() =>navigate('Listing')} style={styles.box3}>
                  <Text style={styles.boxnumber}>44</Text>
                  <Text style={styles.boxtitle}>Exams</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() =>navigate('Listing')} style={styles.box4}>
                  <Text style={styles.boxnumber}>31</Text>
                  <Text style={styles.boxtitle}>Attendance</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

              </View>

              <View style={styles.flexboxcode}>
                {/* <TouchableOpacity onPress={() =>navigate('Listing')} style={styles.box5}>
                  <Text style={styles.boxnumber}>26</Text>
                  <Text style={styles.boxtitle}>Reports</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity> */}

                <TouchableOpacity onPress={() =>navigate('Listing')} style={styles.box6}>
                  <Text style={styles.boxnumber}>65</Text>
                  <Text style={styles.boxtitle}>Settings</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>
              </View>

              </View>

            </View>

           </ScrollView>


     

</SafeAreaView>
        
        
    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,     
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
  color:"#ffffff", fontWeight:"bold", fontSize:30, marginBottom:5,
  },

  boxtitle: {
  color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
  color:"#ffffff", fontWeight:"500", fontSize:12,
  },
  container: {
  paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
  flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop:10, marginRight:10, padding:10, height: 150, paddingTop:20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop:10, marginRight:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%',  marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop:10, marginRight:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%',  marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#dd4b39',
  },

})


