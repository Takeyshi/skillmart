import React, { Component } from 'react';
import {
  Text, View, AppRegistry, Alert, StatusBar, ScrollView, KeyboardAvoidingView,
  StyleSheet, SafeAreaView, Button, TouchableOpacity, Picker, TextInput, Header, Image
} from 'react-native';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import { FontAwesome } from '@expo/vector-icons';

var adharArray = [];
var photoArray = [];
export default class Adddetails extends Component {


  constructor(props) {
    super(props)
    this.state = {
      branch_no: '',
      batch_no: '',
      participant_name: null,
      mobile: null,
      alterphone: null,
      aadharno: null,
      course_no: null,
      email: null,
      data: [],
      address: null,
      branch: [],
      batch: [],
      aadharurl: null,
      photourl: null,
      date: new Date().getDate(),
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      hours: new Date().getHours(),
      min: new Date().getMinutes(),
    }
  }

  loaddata() {

    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/option', {
      method: 'GET'
    })
      .then((response) => response.json())
      .then((responseJson) => {
       // console.log(responseJson.courses);
        this.setState({
          branch: responseJson.branch,
          batch: responseJson.batch,
          data: responseJson.courses
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  componentDidMount() {
    this.loaddata(), this.getPermissionAsync();
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({
        participant_name: null,
        mobile: null,
        alterphone: null,
        aadharno: null,
        course_no: null,
        email: null,
        data: [],
        branch: [],
        batch: [],
        address: null,
        aadharurl: null,
        photourl: null,
        language1: 'Select Batch',
        language2: 'Select Course'
      })
      var adharArray = [];
      var photoArray = [];

    });
  }
  componentDidUpdate() {
    this.state.photourl;
    this.state.aadharurl;
    var photo1 = photoArray.toString();
    var adhar_photo1 = adharArray.toString();
    var ABC = photo1.slice(photo1.length - 20);
    var DEF = adhar_photo1.slice(adhar_photo1.length - 20);
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  _aadhar = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [5, 3],
      quality: 1,
      base64: true,
    });
    console.log(result);
    if (!result.cancelled) {
      this.setState({ aadharurl: result.base64 });
    }
    adharArray.push(this.state.aadharurl.toString())
  };
  _photo = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
      base64: true,
    });
    console.log(result);
    if (!result.cancelled) {
      this.setState({ photourl: result.base64 });
    }
    photoArray.push(this.state.photourl.toString())
  };
  Send_Data_Function(id, name, course1,branch_no,batch_no,course_no) {
    console.log(id, name, course1,branch_no,batch_no,course_no)
    this.props.navigation.navigate('Firsttimefee', {
     
      id: id,
      name: name,     
      coursename: course1,
      branch_no:branch_no,
      batch_no:batch_no,
      course_no:course_no,
    });

  }


  InsertDataToServer = () => {
    const { navigate } = this.props.navigation;
    
    const { participant_name } = this.state;
    const { mobile } = this.state;
    const { alterphone } = this.state;
    const { aadharno } = this.state;
    const { email } = this.state;
    const { address } = this.state;
    const formData = new FormData
    formData.append('branch_no', this.props.navigation.state.params.branchid);
    //formData.append('batch_no', this.state.language1);
    formData.append('course_no', this.state.language2);
    formData.append('participant_name', participant_name);
    formData.append('mobile', mobile);
    formData.append('alterphone', alterphone);
    formData.append('aadharno', aadharno);
    formData.append('email', email);
    formData.append('adhar_photo', this.state.aadharurl);
    formData.append('address', address);
    formData.append('photo', this.state.photourl);

    var current = this.state.date + "/" + this.state.month + "/" + this.state.year + " " + this.state.hours + ":" + this.state.min;

    console.log("Add Student",formData)
    //console.log(this.props.navigation.state.params.branchid)
    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/register', {
      method: 'POST',
      body: formData,
      
    }).then((response) => response.json())

      .then((responseJson) => {
        //console.log("Add Student",responseJson)
        if (responseJson.status === true) {
          this.props.navigation.navigate('Listing')
          Alert.alert(      
            'SkillMart',
            'New Participant Created successfully, Your OTP is "9867". Would Participant like to pay Fee.',     
            [
              {
                text: 'No',
                onPress: () =>  navigate('Listing'),
                style: 'cancel',
              },
              { text: 'Yes',        
              onPress: () => this.Send_Data_Function( 1,this.state.participant_name,this.state.language2,this.state.language,this.state.language1,this.state.courseno),
            },
            ],
            { cancelable: false }
          
          );
        } else {
          alert("Something Went Wrong ,Please try agin.")
        }
      })

      .catch((error) => {
        console.error(error);
      });
  }


  render() {
    const { navigate } = this.props.navigation;

    var photo1 = photoArray.toString();
    var adhar_photo1 = adharArray.toString();
    var ABC = photo1.slice(photo1.length - 20);
    var DEF = adhar_photo1.slice(adhar_photo1.length - 20);


    return (


      <KeyboardAvoidingView behavior="padding" style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>
{console.log(this.state.language2 + this.props.navigation.state.params.branchid)}

        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Listing')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={25} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Add New Participant</Text>
          </View>
          {/* <TouchableOpacity style={{ height: '100%', width: '15%', justifyContent: "center", marginTop: 10, alignItems: "center" }}>
            <FontAwesome name="save" size={25} color="#ffffff" />
          </TouchableOpacity> */}
        </View>


        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Branch</Text>
                </View>
                <View style={{ justifyContent: "center", width: '50%', marginTop: -10, }}>

                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: 170, fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.props.navigation.state.params.branchid}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language: itemValue, choosenIndex: itemPosition, })} >

                    {this.state.branch.map((item, key) => (

                      <Picker.Item label={item.branch_name} value={item.branch_id} key={key} />)

                    )}

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Course</Text>
                </View>
                <View style={{ justifyContent: "center", width: '50%', marginTop: -10, }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: 170, fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language2}
                    onValueChange={(itemValue, itemLable) =>
                      this.setState({ language2: itemValue, courseno: itemLable })}
                  >
                    <Picker.Item label="Select Course" value="S0" />
                    {this.state.data.map((item, key) => (
                      <Picker.Item label={item.course_name} value={item.course_id} key={key} />)
                    )}
                  </Picker>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Participant Name </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                      onChangeText={(participant_name) => this.setState({ participant_name })}
                      value={this.state.participant_name}
                      placeholder="Enter Participant Name"
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Mobile Number</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(mobile) => this.setState({ mobile })}
                    value={this.state.mobile}
                    placeholder="Enter Mobile Number"
                  />
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Alternate Phone</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(alterphone) => this.setState({ alterphone })}
                    value={this.state.alterphone}
                    placeholder="Enter Alternate Number"

                  />
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Aadhar Number </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(aadharno) => this.setState({ aadharno })}
                    value={this.state.aadharno}
                    placeholder="Enter Aadhar Number"
                  />
                </View>
              </View>
            </View>
          </View>

          <TouchableOpacity onPress={this._aadhar} style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Upload Aadhar Photo  </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>{DEF.length == 0 ? " :- Upload Aadharcar" : DEF}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this._photo} style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Upload Photo  </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>

                  <Text style={styles.listingmaintitledetails}>{ABC.length == 0 ? " :- Upload Photo" : ABC}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <View style={styles.listingtouch}>
            <View style={{ padding: 20, flexDirection: 'row', height: 100 }}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "center", width: '50%', alignItems: "flex-start", }}>
                  <Text style={styles.listingmaintitledetails}>Enter Address  </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 80, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(address) => this.setState({ address })}
                    value={this.state.address}
                    placeholder="Enter permanent address"
                    maxLength={210}
                  />
                </View>
              </View>
            </View>
          </View>





          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Email Address </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(email) => this.setState({ email })}
                    value={this.state.email}
                    placeholder="Enter Email Address"
                  />
                </View>
              </View>
            </View>
          </View>


        </ScrollView>

        <Button title="Save Information" onPress={this.InsertDataToServer} color="#2196F3" />

      </KeyboardAvoidingView>



    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },


  mainviewdetails: { padding: 20, flexDirection: 'row', height: 60 },

  listingmainpriority: { color: 'green', fontWeight: '500', fontSize: 16 },

  listingtouchgrey: {
    backgroundColor: '#f9f9f9', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },

  listingmaintitledetails: { color: 'black', fontWeight: '500', fontSize: 15, textAlign: 'center' },
  listingmainttext: { color: 'black', fontWeight: '400', fontSize: 15 },

})




