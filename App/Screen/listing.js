/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, { Component } from 'react';
import { Text, View, AppRegistry, StatusBar, ScrollView, FlatList, StyleSheet, SafeAreaView, TouchableOpacity, TextInput, Header, Image, ActivityIndicator } from 'react-native';
// import Icon from './node_modules/react-native-vector-icons/FontAwesome';
// import Icon1 from './node_modules/react-native-vector-icons/Entypo';
import { FontAwesome } from '@expo/vector-icons';



export default class listing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataSource: [],
      pageid: [],
      Firstletter: [],


    };
  }
  loaddata() {

    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/Student/detail', { method: 'GET' })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({
          loading: false,
          dataSource: responseJson
        })
      })

      //Catch if any error here
      .catch(error => console.log(error))
  }
  //Fatch Api here
  componentDidMount() { this.loaddata() }
  componentWillMount() { this.loaddata() }
  componentDidUpdate() { this.loaddata() }

  Send_Data_Function(id, name, mobile, alter, aadhar, courseno, Email, ) {

    this.props.navigation.navigate('Details', {
      id: id,
      name: name,
      number: mobile,
      alter: alter,
      aadhar: aadhar,
      courseno: courseno,
      Email: Email,

    });

  }




  renderItem = (data) =>

    <TouchableOpacity
      onPress={() => this.Send_Data_Function(data.item.party_id,
        data.item.participant_name, data.item.mobile, data.item.alterphone,
        data.item.aadharno, data.item.course_name, data.item.email, this.props.navigation.getParam.branch)}
      style={styles.listingtouch}>
      <View style={styles.mainviewlisting}>
        <View style={styles.circlered}>
          <Text style={styles.circletitle}>{data.item.participant_name.charAt(0)}</Text>
        </View>
        <View style={styles.textviewwidth} >
          <View style={styles.titlespacing}>
            <Text style={styles.listingmaintitle}>{data.item.participant_name}</Text>
          </View>
          <View>
            <Text style={styles.approvestatus}>{data.item.course_name}  </Text>
          </View>
        </View>
        <View style={styles.listingrightarrow}>
          <FontAwesome name="chevron-right" size={20} color="#555555" />
        </View>
      </View>
    </TouchableOpacity>



  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#0c9" />
        </View>
      )
    }
    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>


        <View style={{ height: '10%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Home')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={30} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Register Participants</Text>
          </View>
          <TouchableOpacity onPress={() => navigate('Adddetails', { branchid: this.props.navigation.state.params.branchid })} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="plus" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>


        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>

          <FlatList
            data={this.state.dataSource}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={item => this.renderItem(item)}
            keyExtractor={item => item.party_id.toString()} />

        </ScrollView>

      </SafeAreaView>


    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
})




