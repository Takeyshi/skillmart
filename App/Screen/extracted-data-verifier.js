import React, { Component } from 'react';
import { Text, View, AppRegistry, Alert, StatusBar, ScrollView, FlatList, StyleSheet, SafeAreaView, Button, TouchableOpacity, TextInput, Header, Image } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';



export default class Extracteddataverifier extends Component {

    showAlert1() {  
        Alert.alert(  
            'Delete Entry',  
            'Are you sure you want to delete this entry?',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'OK', onPress: () => console.log('OK Pressed')},  
            ]  
        );  
    }  

  render() {
    const { navigate } = this.props.navigation;

    return (
     
      


<SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>

        
          <View style={{height: '12%', flexDirection: 'row', backgroundColor: '#0186C2'}} >
              <TouchableOpacity  onPress={() =>navigate('Verifierdetails')} style={{height: '100%', width: '15%', marginTop:10, justifyContent: "center", alignItems: "center"}}>
                  <FontAwesome name="angle-left" size={25} color="#ffffff" />
              </TouchableOpacity>
              <View style={{height: '100%', width: '70%', marginTop:10, justifyContent: "center", alignItems: "center"}} >
                  <Text style={{color:"#ffffff", fontSize:18, fontWeight:"bold"}}>Extracted Invoice #829</Text>
              </View>
          </View>

          
          <ScrollView style={{backgroundColor: '#ffffff',  paddingBottom: '3%'}}>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'column', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", alignItems: "flex-start"}}>
                  <Text style={{color:"#d26a5c", fontSize:18, fontWeight:"bold"}}>
                  Summary</Text>
                </View>

              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Date Issue</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="2019-04-08" style={{height: 40, paddingLeft:5, borderWidth:1, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Amount Total</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="24773.00" style={{height: 40, paddingLeft:5, borderWidth:1, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Amount Due</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="24773.00"  style={{height: 40, paddingLeft:5, borderWidth:1, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
              </View>
              </View>
              </View>





            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'column', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", alignItems: "flex-start"}}>
                  <Text style={{color:"#d26a5c", fontSize:18, fontWeight:"bold"}}>Invoice Details</Text>
                </View>

              </View>
              </View>
              </View>



            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails} style={{color:"#d26a5c", fontSize:16, fontWeight:"bold"}}>Product 1</Text>
                </View>
                <View style={{justifyContent: "flex-end", width:'45%', alignItems: "flex-end"}}>
             
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Description</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="Electricity Charges" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Amount</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="New Chairs" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Quantity</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="Air Conditioner" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Amount Total</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="1200.00" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
              </View>
              </View>
              </View>



            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails} style={{color:"#d26a5c", fontSize:16, fontWeight:"bold"}}>Product 2</Text>
                </View>
                <View style={{justifyContent: "flex-end", width:'45%', alignItems: "flex-end"}}>
           
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Description</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="Electricity Charges" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Amount</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="New Chairs" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Quantity</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="Air Conditioner" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Amount Total</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', marginTop:-10, alignItems: "flex-start"}}>
                  <TextInput value="1200.00" style={{height: 40, borderWidth:1, paddingLeft:5, borderColor:'#dddddd', backgroundColor:'#ffffff', width:150}} />
                </View>
              </View>
              </View>
              </View>




            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Total</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>24773.00</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', color:"#d26a5c", alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Verified by </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Lisa Jenkins</Text>
                </View>
              </View>
              </View>
              </View>


              <View style={styles.listingtouchgrey}>
                <View style={styles.mainviewdetails}>

                  <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                    <TouchableOpacity  onPress={() =>navigate('Verifierdetails')} style={{justifyContent: "center", width:'100%', backgroundColor:"#dd4b39", marginRight:5, paddingTop:12, paddingBottom:5, borderRadius:5,  paddingLeft:5, paddingRight:5, alignItems: "center"}}>
                      <Text style={styles.listingmaintitledetails} style={{color:"#ffffff", fontSize:16, fontWeight:"bold"}}>Back</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>


           </ScrollView>


     

</SafeAreaView>
        
        
    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,     
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
  color:"#ffffff", fontWeight:"bold", fontSize:30, marginBottom:5,
  },

  boxtitle: {
  color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
  color:"#ffffff", fontWeight:"500", fontSize:12,
  },
  container: {
  paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
  flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop:10, marginRight:10, padding:10, height: 150, paddingTop:20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop:10, marginRight:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%',  marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop:10, marginRight:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%',  marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40/2, marginRight:10,
    backgroundColor: '#dd4b39',alignItems:'center', justifyContent:'center',
},
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40/2, marginRight:10,
    backgroundColor: '#00a65a',alignItems:'center', justifyContent:'center',
},
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40/2, marginRight:10,
    backgroundColor: '#f39c12',alignItems:'center', justifyContent:'center',
},
  listingtouch: {
    backgroundColor:'#ffffff', borderBottomWidth:1, borderColor:'#e6e6e6', alignItems:'center',
  },
  circletitle: {color:'white',fontWeight:'bold',fontSize:20},
  listingmaintitle: {color:'black',fontWeight:'500',fontSize:16},
  approvestatus: {color:'#00a65a', fontSize:13},
  listingrightarrow: {width: '15%', justifyContent: "center", alignItems: "center"},
  mainviewlisting: {padding:20,flexDirection:'row', height:80},
  textviewwidth: {width: '75%'},
  titlespacing: {marginBottom:5},


  mainviewdetails: {padding:20,flexDirection:'row', height:60},

  listingmainpriority: {color:'green',fontWeight:'500',fontSize:16},

  listingtouchgrey: {
    backgroundColor:'#f9f9f9', borderBottomWidth:1, borderColor:'#e6e6e6', alignItems:'center',
  },

  listingmaintitledetails: {color:'black',fontWeight:'500',fontSize:15},
  listingmainttext: {color:'black',fontWeight:'400',fontSize:15},

})




