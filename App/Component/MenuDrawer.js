import React from 'react';
import {
    View,
    Text,
    platform,
    SafeAreaView,
    Image,
    Linking,
    ImageBackground,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
} from 'react-native';


import { FontAwesome5 } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons'; Entypo

const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height

export default class MenuDrawer extends React.Component {

    constructor() {
        super();
        this.state = {
            status: true,
            status1: true,
            status2: true
        }
    }
    ShowHideTextComponentView = () => {
        if (this.state.status == true) {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }
    ShowHideTextComponentView1 = () => {
        if (this.state.status1 == true) {
            this.setState({ status1: false })
        }
        else {
            this.setState({ status1: true })
        }
    }
    ShowHideTextComponentView2 = () => {
        if (this.state.status2 == true) {
            this.setState({ status2: false })
        }
        else {
            this.setState({ status2: true })
        }
    }

    navLink(nav, text) {
        return (
            <TouchableOpacity style={{ height: 45 }} onPress={() => this.props.navigation.navigate(nav)} >
                <Text style={styles.link}>{text}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        return (

            <View style={styles.container}>
                <View style={styles.toplinks}>
                    <View style={{ flex: 1, margin: 20, marginBottom: 20 }}>
                        <ImageBackground style={{ height: 70, justifyContent: 'center', margin: 15 }} source={require('./img/logo-white.png')}>
                        </ImageBackground>
                    </View>
                </View>


                <View style={styles.bottomlinks}>
                    <View style={{ flexDirection: 'row' }}>
                        <FontAwesome5 name="home" color="white" size={20} style={styles.menuIcon}
                            onPress={() => { }} />
                        <View style={styles.linkicon}>
                            {this.navLink('Home', 'Dashboard')}
                        </View>
                        {/* <FontAwesome5 name="chevron-right" color="white" size={21} style={{ marginStart: 10, top: 10 }}
                            onPress={this.ShowHideTextComponentView} /> */}
                    </View>
                    {/*Start*/}
                    {this.state.status ?
                        <View>
                            <View style={{ flexDirection: 'row' }} >
                                <FontAwesome5 name="file-invoice" color="white" size={20} style={styles.menuIcon0}
                                    onPress={() => { }} />
                                <View style={styles.linkicon0}>
                                    {this.navLink('Feelist', 'Student Fees')}
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row' }} >
                                <Entypo name="user" color="white" size={20} style={styles.menuIcon0}
                                    onPress={() => { }} />
                                <View style={styles.linkicon0}>
                                    {this.navLink('Listing', 'Manage Students')}
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row' }} >
                                <Entypo name="flow-branch" color="white" size={20} style={styles.menuIcon0}
                                    onPress={() => { }} />
                                <View style={styles.linkicon0}>
                                    {this.navLink('Examlist', 'Manage Exams')}
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row' }} >
                                <Entypo name="user" color="white" size={20} style={styles.menuIcon0}
                                    onPress={() => { }} />
                                <View style={styles.linkicon0}>
                                    {this.navLink('Attendancelist', 'Students Attendance')}
                                </View>
                            </View>

                        </View> : null}
                    {/*End*/}

                </View>
               
            </View>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lightgray',
    },
    toplinks: {
        height: 120,
        backgroundColor: '#ffffff',


    },
    bottomlinks: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 440,
        backgroundColor: "#018dc8",
    },
    link: {
        flex: 1,
        fontSize: 17,
        color: 'white',
        padding: 6,
        paddingLeft: 14,
        margin: 5,
        textAlign: 'left',
    },

    social: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    linkicon: {
        flexDirection: 'row',
        marginLeft: 20,
        fontWeight: 'bold'

    },
    menuIcon: {
        zIndex: 9,
        position: 'absolute',
        top: 10,
        left: 10,
    },
    menuIcon0: {
        zIndex: 9,
        position: 'absolute',
        top: 10,
        left: 60,
    },
    linkicon0: {
        flexDirection: 'row',
        marginLeft: 65,
        top: 1,

    },

    footer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
    },
    description: {
        flex: 1,
        marginLeft: 20,
        fontSize: 16
    },
    version: {
        flex: 1,
        textAlign: 'right',
        marginRight: 20,
        color: 'gray',
    },



})