import React from "react";
import { StyleSheet, View, ActivityIndicator, FlatList, Text, TouchableOpacity, SafeAreaView } from "react-native";
import { Icon } from "react-native-elements";
import { FontAwesome } from '@expo/vector-icons';


var newArray = [];
var SampleArray = [];
var AbArray = [];
var AbArray1 = [];

export default class Store extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      dataSource: [],
      Sample: [],
      AbArray: [],
      AbArray1: [],
      newArray: [],
      userid: [],
      brachid: this.props.navigation.state.params.branch1,
      batchid: this.props.navigation.state.params.batch1,
      courseid: this.props.navigation.state.params.course1,
      studentid: [],
      currentdate: new Date().getDate(),
      currentmonth: new Date().getMonth() + 1,
      currentyear: new Date().getFullYear(),
    };
  }

  componentDidMount() {
    this.fetchData();

  }

  InsertDataToServer = () => {

  }

  fetchData = () => {
    this.setState({ loading: true });

    fetch("http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/Student/detail")
      .then(response => response.json())
      .then(responseJson => {
        //console.log(responseJson.party_id, "oldest up")
        responseJson = responseJson.map(item => {
          item.isSelect = false;
          item.selectedClass = styles.list;
          this.setState({ Sample: item.party_id })
          return item;
        });
        this.setState({
          loading: false,
          dataSource: responseJson,

        });
        //console.log("================>>>>>>>>>>>>>>>>>>>>>>>>", SampleArray.push(this.state.Sample.party_id.toString()));

      })

      .catch(error => {
        this.setState({ loading: false });
      });
  };

  FlatListItemSeparator = () => <View style={styles.line} />;






  selectItem = (data, id) => {

    this.setState({ studentid: id })
    console.log("dt", data);
    data.item.isSelect = !data.item.isSelect;
    data.item.selectedClass = data.item.isSelect ? styles.selected : styles.list;
    data.item.party_id = data.item.isSelect ? newArray.push(data.item.party_id.toString()) : newArray.splice(newArray.indexOf(data.item.party_id), 1);
    //data.item.party_id = data.item.isSelect ? SampleArray.splice(SampleArray.indexOf(data.item.party_id), 1): null ;

    const index = this.state.dataSource.findIndex
      (
        item => data.item.party_id === item.party_id
      );

    this.state.dataSource[index] = data.item;
    this.setState({ dataSource: this.state.dataSource, studentid: id });

  };


  goToStore() {
    const AbArray1 = AbArray.filter(item => !newArray.includes(item))        
    // console.log("new", newArray)
    // console.log("Ab ===>", AbArray1.slice(1))
    // console.log(this.props.navigation.state.params.branch1 + " " + this.props.navigation.state.params.batch1 + " " + this.props.navigation.state.params.course1)
    const formData = new FormData
    formData.append('branch_no', this.props.navigation.state.params.branch1);
    formData.append('batch_no', this.props.navigation.state.params.batch1);
    formData.append('course_no', this.props.navigation.state.params.course1);
    formData.append('participate_absent_ids', AbArray1.slice(1).toString());
    formData.append('participate_present_ids', newArray.toString());
    console.log("===================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", formData)
    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/addattendance', {
      method: 'POST',
      body: formData,
    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === true) {
          alert("Attendance saved")
          this.props.navigation.navigate('Selections')

        } else {
          alert("Something Went Wrong ,Please try agin.")
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  renderItem = data =>

    <TouchableOpacity
      style={[styles.list, data.item.selectedClass]}
      onPress={() => this.selectItem(data, data.item.party_id)}>
      <View style={styles.mainviewlisting}>
        <View style={[styles.circlered]}>
          <Text style={styles.circletitle}>{data.item.participant_name.charAt(0)}</Text>
        </View>
        <View style={styles.textviewwidth} >
          <View style={styles.titlespacing}>
            <Text style={styles.listingmaintitle}>{data.item.participant_name}</Text>
          </View>
          <View>
            <Text style={styles.approvestatus}>Course: {this.state.courseid} | Date: {this.state.currentdate + "-" + this.state.currentmonth + "-" + this.state.currentyear}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity >



  render() {
    const { navigate } = this.props.navigation;    
    SampleArray.push(this.state.Sample.toString())
    SampleArray.slice(2)
    AbArray = SampleArray.filter(function (item, index, SampleArray) {
      return SampleArray.indexOf(item) == index;
    });    
    // console.log("1", SampleArray.slice(1).toString())
    // console.log("2", AbArray.toString())
    // console.log("3", newArray.toString())
    const itemNumber = this.state.dataSource.filter(id => id.isSelect).length;

    if (this.state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="purple" />
        </View>
      );
    }

    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>


        <View style={{ height: '10%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Home')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={30} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Participants Attendance</Text>
          </View>
          <TouchableOpacity onPress={() => {
            Alert.alert(
              'Entry Saved',
              'Your details are saved successfully.',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                { text: 'OK', onPress: () => navigate("Home") },
              ]
            );
          }} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="save" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={item => this.renderItem(item)}
          keyExtractor={item => item.party_id.toString()}
          extraData={this.state}
        />

        <View style={styles.numberBox}>
          <Text style={styles.number}>{itemNumber}</Text>
        </View>

        <TouchableOpacity style={styles.icon}>
          <View>
            <Icon
              raised
              name="user"
              type="font-awesome"
              color="#e3e3e3"
              size={30}
              onPress={() => this.goToStore()}
              containerStyle={{ backgroundColor: "#FA7B5F" }}
            />
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingVertical: 50,
    position: "relative"
  },
  title: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    marginBottom: 10
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  list: {
    paddingVertical: 2,
    margin: 3,
    flexDirection: "row",
    backgroundColor: "#E8E9EB",
    justifyContent: "flex-start",
    alignItems: "center",
    zIndex: -1
  },
  lightText: {
    color: "#171717",
    width: 200,
    paddingLeft: 15,
    fontSize: 17,
    fontWeight: 'bold'
  },
  line: {
    height: 0.9,
    width: "100%",
    backgroundColor: "black"
  },
  icon: {
    position: "absolute",
    bottom: 20,
    width: "100%",
    left: 290,
    zIndex: 1
  },
  numberBox: {
    position: "absolute",
    bottom: 75,
    width: 30,
    height: 30,
    borderRadius: 15,
    left: 330,
    zIndex: 3,
    backgroundColor: "#e3e3e3",
    justifyContent: "center",
    alignItems: "center"
  },
  number: { fontSize: 14, color: "#000" },
  selected: { backgroundColor: "#FA7B5F" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  circlered: {
    width: 45,
    height: 45,
    backgroundColor: 'green',
    borderRadius: 45 / 2, marginRight: 10,
    alignItems: 'center', justifyContent: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: 'black', fontSize: 13 },
});




