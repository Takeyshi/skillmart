import React, { Component } from 'react';
import { Text, View, AppRegistry, Alert, StatusBar, ScrollView, FlatList, StyleSheet, SafeAreaView, KeyboardAvoidingView, Picker, TouchableOpacity, TextInput, Header, Image } from 'react-native';

import { FontAwesome } from '@expo/vector-icons';
export default class Adddetails extends Component {
  constructor(props) {
    super(props)

    this.state = {
      fee_id: '',
      Install_1: '',
      Install_2: '',
      Install_3: '',
      Install_4: '',
      Install_5: '',
      Install_6: '',     

    }
  }
  

  componentDidMount() {
    const { navigation } = this.props;
    console.log(this.props.navigation.state.params.installment_1)
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({      
        Install_1: this.props.navigation.state.params.installment_1,
        language1:'payment',
        edit1: this.props.navigation.state.params.installment_1.lenghth > 1 ? "flase" : "true",
        Install_2: this.props.navigation.state.params.installment_2,
        language2:'payment',
        Install_3: this.props.navigation.state.params.installment_3,
        language3:'payment',
        Install_4: this.props.navigation.state.params.installment_4,
        language4:'payment',
        Install_5: this.props.navigation.state.params.installment_5,
        language5:'payment',
        Install_6: this.props.navigation.state.params.installment_6,
        language6:'payment',

      })
    });
  }
  // componentDidUpdate() {this.loaddata()}

  InsertDataToServer = () => {
    const formData = new FormData
    formData.append('fee_id', this.props.navigation.state.params.id);
    formData.append('install_1', this.state.Install_1);
    formData.append('install_2', this.state.Install_2);
    formData.append('install_3', this.state.Install_3);
    formData.append('install_4', this.state.Install_4);
    formData.append('install_5', this.state.Install_5);
    formData.append('install_6', this.state.Install_6);
    formData.append('install_1_method', this.state.language1);
    formData.append('install_2_method', this.state.language2);
    formData.append('install_3_method', this.state.language3);
    formData.append('install_4_method', this.state.language4);
    formData.append('install_5_method', this.state.language5);
    formData.append('install_6_method', this.state.language6);

    //Add Api Here
    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/updatefees', {
      method: 'POST',
      body: formData,

    }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.status)
        console.log("Installment response" + responseJson)
        if (responseJson.status === true) {
          this.props.navigation.navigate('Feelist')
        } else {
          alert("Something Went Wrong ,Please try agin.")
        }
      })

      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    const { goBack } = this.props.navigation;
    return (


      <KeyboardAvoidingView
        behavior="padding"
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'stretch', backgroundColor: '#0186C2',
        }}>


        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Feedetails')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={25} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Edit Fees Details</Text>
          </View>
          <TouchableOpacity onPress={this.InsertDataToServer} style={{ height: '100%', width: '15%', justifyContent: "center", marginTop: 10, alignItems: "center" }}>
            <FontAwesome name="save" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>


        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Fee ID</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                    <Text style={styles.listingmaintitledetails}>{this.props.navigation.state.params.id}</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Full Name </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                    <Text style={styles.listingmainttext}>{this.props.navigation.state.params.name}</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Installment 1</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(Install_1) => this.setState({ Install_1 })}                    
                    value={this.state.Install_1}
                    placeholder="First Install_" />
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Payments-method</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: '100%', fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language1}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language1: itemValue, Payments1: itemPosition })}
                  >
                    <Picker.Item label="payment" value="payment" />
                    <Picker.Item label="Cash" value="Cash" />
                    <Picker.Item label="Cheque" value="Cheque" />

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Installment  2</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(Install_2) => this.setState({ Install_2 })}
                    value={this.state.Install_2}
                    placeholder="Second Install_" />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Payments-method</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: '100%', fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language2}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language2: itemValue, Payments2: itemPosition })}
                  >
                    <Picker.Item label="payment" value="" />
                    <Picker.Item label="Cash" value="Cash" />
                    <Picker.Item label="Cheque" value="Cheque" />

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Installment 3 </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(Install_3) => this.setState({ Install_3 })}
                    value={this.state.Install_3}
                    placeholder="Third Install_" />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Payments-method</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: '100%', fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language3}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language3: itemValue, Payments3: itemPosition })}
                  >
                    <Picker.Item label="payment" value="" />
                    <Picker.Item label="Cash" value="Cash" />
                    <Picker.Item label="Cheque" value="Cheque" />

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Installment 4 </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(Install_4) => this.setState({ Install_4 })}
                    value={this.state.Install_4}
                    placeholder="Forth Install_" />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Payments-method</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: '100%', fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language4}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language4: itemValue, Payments4: itemPosition })}
                  >
                    <Picker.Item label="payment" value="" />
                    <Picker.Item label="Cash" value="Cash" />
                    <Picker.Item label="Cheque" value="Cheque" />

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Installment 5 </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(Install_5) => this.setState({ Install_5 })}
                    value={this.state.Install_5}
                    placeholder="Fifth Install_" />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Payments-method</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: '100%', fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language5}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language5: itemValue, Payments5: itemPosition })}
                  >
                    <Picker.Item label="payment" value="" />
                    <Picker.Item label="Cash" value="Cash" />
                    <Picker.Item label="Cheque" value="Cheque" />

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Installment 6</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }}
                    onChangeText={(Install_6) => this.setState({ Install_6 })}
                    value={this.state.Install_6}
                    placeholder="Sixth Install_" />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Payments-method</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: '100%', fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language6}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language6: itemValue, Payments6: itemPosition })}
                  >
                    <Picker.Item label="payment" value="" />
                    <Picker.Item label="Cash" value="Cash" />
                    <Picker.Item label="Cheque" value="Cheque" />

                  </Picker>
                </View>
              </View>
            </View>
          </View>


        </ScrollView>



      </KeyboardAvoidingView>



    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },


  mainviewdetails: { padding: 20, flexDirection: 'row', height: 60 },

  listingmainpriority: { color: 'green', fontWeight: '500', fontSize: 16 },

  listingtouchgrey: {
    backgroundColor: '#f9f9f9', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },

  listingmaintitledetails: { color: 'black', fontWeight: '500', fontSize: 15 },
  listingmainttext: { color: 'black', fontWeight: '400', fontSize: 15 },

})




