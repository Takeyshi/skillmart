import React, { Component } from 'react';
import { Text, TextInput, View, Button, KeyboardAvoidingView, Image } from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      data:'',
    };
  }
  GetValueFunction = () => {
    if (this.state.username != null) {
      if (this.state.password != null) {
        const { username, password } = this.state;
        const formData = new FormData
        formData.append('uname', username);
        formData.append('password', password);

        fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/login', {
          method: 'POST',
          body: formData,

        }).then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === true) {
              this.setState({
                data: responseJson.user_info.branch_id
              })
              { console.log("Login", this.state.data) }
              this.props.navigation.navigate('Home', {branch:this.state.data })
             
            } else {
              alert("Something Went Wrong ,Please try agin.")
            }
          })
          .catch((error) => {
            console.error(error);
          });
        // alert(username + password);
      }
    }
    else {
      alert("Something went wrong, please try again")
    }


  };
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <KeyboardAvoidingView style={{ height: '100%', width: '80%', flex: 1, justifyContent: 'center', }}
          behavior='padding'>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10, padding: 5 }}>
            <Image
              style={{ width: "100%", height: 80 }}
              source={require('./image/Welcome.png')}
            />
          </View>
          <TextInput
            style={{ height: 50, borderColor: 'black', borderWidth: 2, padding: 5 }}
            placeholder="Username"
            onChangeText={username => this.setState({ username })}
            value={this.state.username}
          />
          <TextInput
            style={{ height: 50, borderColor: 'black', borderWidth: 2, padding: 5, marginTop: 8 }}
            placeholder="Passwprd"
            secureTextEntry={true}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
          <View style={{ marginTop: 12, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Button title="Login" onPress={this.GetValueFunction} color="#2196F3" />
          </View>

        </KeyboardAvoidingView>
      </View>
    );
  }
}
