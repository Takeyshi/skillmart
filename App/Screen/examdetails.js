import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, SafeAreaView, TouchableOpacity, Alert, } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';


export default class Details extends Component {
  state = {
    dataSource: '',
  }
  
  loaddata() {
    const formData = new FormData
    formData.append('party_id', this.props.navigation.state.params.id);
    formData.append('exam_id', this.props.navigation.state.params.examid);



    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/getstudentexammarks', {
      method: 'POST',
      body: formData
    })
      .then(response => response.json())
      .then((responseJson) => {
        console.log(responseJson.total_marks)
        this.setState({
          loading: false,
          dataSource: responseJson,

        })
        console.log(this.state.dataSource)
      })
      //Catch if any error here
      .catch(error => console.log("Exam" + error))
  }
  componentDidMount() { this.loaddata() }
  componentDidUpdate() { this.loaddata() }

  Send_Data_Function(examid, id, name, total, obtained) {

    this.props.navigation.navigate('Editexamdetails', {
      examid: examid,
      id: id,
      name: name,
      total: total,
      obtained: obtained,

    });

  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>


        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Examlist')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={30} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Exam Details</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.Send_Data_Function(this.props.navigation.state.params.examid, this.props.navigation.state.params.id,
              this.props.navigation.state.params.name, this.props.navigation.state.params.total, this.state.dataSource.obtained_marks)} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="edit" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>


        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Participant ID </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>{this.props.navigation.state.params.id}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Full Name</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.name}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Course Name </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.course}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Obtained Marks</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.state.dataSource.obtained_marks}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Total Marks</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.total}</Text>
                </View>
              </View>
            </View>
          </View>


        </ScrollView>




      </SafeAreaView>


    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },


  mainviewdetails: { padding: 20, flexDirection: 'row', height: 60 },

  listingmainpriority: { color: 'green', fontWeight: '500', fontSize: 16 },

  listingtouchgrey: {
    backgroundColor: '#f9f9f9', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },

  listingmaintitledetails: { color: 'black', fontWeight: '500', fontSize: 15 },
  listingmainttext: { color: 'black', fontWeight: '400', fontSize: 15 },

})




