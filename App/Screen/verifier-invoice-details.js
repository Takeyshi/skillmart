import React, { Component } from 'react';
import {
  Animated,
  Dimensions,
  Image,
  LayoutAnimation,
  Platform,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  UIManager,
  View,
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const width = Dimensions.get('window').width;

class Animated_Item extends Component {
  constructor() {
    super();

    this.animatedValue = new Animated.Value(0);

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.item.id !== this.props.item.id) {
      return true;
    }
    return false;
  }

  componentDidMount() {
    Animated.timing(this.animatedValue, {
      toValue: 0.5,
      duration: 510,
      useNativeDriver: true,
    }).start(() => {
      this.props.afterAnimationComplete();
    });
  }

  deleteItem = () => {
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 510,
      useNativeDriver: true,
    }).start(() => {
      this.props.deleteItem(this.props.item.id);
    });
  };

  render() {
    const translate_Animation_Object = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [-width, 0, width],
    });

    const opacity_Animation_Object = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 0],
    });

    {
      /*Work Here*/
    }
    return (
      <Animated.View
        style={[
          styles.singleItemView,
          {
            transform: [{ translateX: translate_Animation_Object }],
            opacity: opacity_Animation_Object,
          },
        ]}>
        <View style={{ flexDirection: 'row', width: '96%', height: 40 }}>


          <TouchableOpacity style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
            <Text style={styles.singleItemText} style={{ color: "#d26a5c", fontSize: 16, fontWeight: "bold" }}>Product {this.props.item.text}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.deleteButton}
            onPress={this.deleteItem}>
            <FontAwesome name="trash" size={25} color="#666666" />
          </TouchableOpacity>
        </View>


        <View style={styles.listingtouch}>
          <View style={styles.mainviewdetails}>
            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
              <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                <Text style={styles.listingmaintitledetails}>Description</Text>
              </View>
              <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.listingtouch}>
          <View style={styles.mainviewdetails}>
            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
              <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                <Text style={styles.listingmaintitledetails}>Amount</Text>
              </View>
              <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.listingtouch}>
          <View style={styles.mainviewdetails}>

            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
              <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                <Text style={styles.listingmaintitledetails}>Quantity</Text>
              </View>
              <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
              </View>
            </View>
          </View>
        </View>
        <View style={styles.listingtouch}>
          <View style={styles.mainviewdetails}>
            <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
              <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                <Text style={styles.listingmaintitledetails}>Amount Total</Text>
              </View>
              <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
              </View>
            </View>
          </View>
        </View>
      </Animated.View>
    );
  }
}

export default class App extends Component {
  constructor() {
    super();
    this.state = { valueArray: [], disabled: false };
    this.addNewElement = false;
    this.index = 0;
  }

  afterAnimationComplete = () => {
    this.index += 1;
    this.setState({ disabled: false });
  };

  add_New_View = () => {
    this.addNewElement = true;
    const newlyAddedValue = { id: 'id_' + this.index, text: this.index + 1 };

    this.setState({
      disabled: true,
      valueArray: [...this.state.valueArray, newlyAddedValue],
    });
  };

  remove_Selected_Item(id) {
    this.addNewElement = false;
    const newArray = [...this.state.valueArray];
    newArray.splice(newArray.findIndex(ele => ele.id === id), 1);

    this.setState(
      () => {
        return {
          valueArray: newArray,
        };
      },
      () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      }
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ height: '10%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Verifierinvoicelisting')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={25} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Invoice #624</Text>
          </View>
          <TouchableOpacity onPress={() => navigate('Verifierinvoicelisting')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="undo" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>
        <ScrollView
          ref={scrollView => (this.scrollView = scrollView)}
          onContentSizeChange={() => {
            this.addNewElement && this.scrollView.scrollToEnd();
          }}>
          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'column', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", alignItems: "flex-start" }}>
                  <Text style={{ color: "#d26a5c", fontSize: 18, fontWeight: "bold" }}>Instructions</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouchgrey}>
            <View style={{ padding: 10 }}>
              <View style={{ flexDirection: 'column', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", alignItems: "flex-start" }}>
                  <Text style={{ color: "#666666", fontSize: 14, marginBottom: 10 }}><FontAwesome name="angle-right" size={18} style={{ color: '#d26a5c' }} color="#666666" /> Date should be in yyyy-MM-DD format.</Text>
                  <Text style={{ color: "#666666", fontSize: 14, marginBottom: 10 }}><FontAwesome name="angle-right" size={18} style={{ color: '#d26a5c' }} color="#666666" /> Make sure that amount_total and amount_due in Summary is correct.</Text>
                  <Text style={{ color: "#666666", fontSize: 14, marginBottom: 10 }}><FontAwesome name="angle-right" size={18} style={{ color: '#d26a5c' }} color="#666666" /> Check all the records in Invoice Details, correct and add all applicable missing items such as line items, taxes, discounts, amount paid.</Text>
                  <Text style={{ color: "#666666", fontSize: 14, marginBottom: 10 }}><FontAwesome name="angle-right" size={18} style={{ color: '#d26a5c' }} color="#666666" /> Make sure that the Total in Invoice Details matches with amount_due in Summary.</Text>
                  <Text style={{ color: "#666666", fontSize: 16, fontWeight: "bold", marginBottom: 10 }}>Pending verification : 2</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'column', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", alignItems: "flex-start" }}>
                  <Text style={{ color: "#d26a5c", fontSize: 18, fontWeight: "bold" }}>
                    Summary</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Date Issue</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Amount Total</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                    <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Amount Due</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
                  <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'column', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", alignItems: "flex-start" }}>
                  <Text style={{ color: "#d26a5c", fontSize: 18, fontWeight: "bold" }}>Invoice Details</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={{ flexDirection: 'row', width: '96%', height: 40 }}>


<TouchableOpacity style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start",marginStart:10,marginTop:10 }}>
  <Text style={styles.singleItemText} style={{ color: "#d26a5c", fontSize: 18, fontWeight: "bold" }}>Product </Text>
</TouchableOpacity>

{/* <TouchableOpacity
  style={styles.deleteButton}
  onPress={this.deleteItem}>
  <FontAwesome name="trash" size={25} color="#666666" />
</TouchableOpacity> */}
</View>


<View style={styles.listingtouch}>
<View style={styles.mainviewdetails}>
  <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
    <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
      <Text style={styles.listingmaintitledetails}>Description</Text>
    </View>
    <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
      <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
        <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
      </View>
    </View>
  </View>
</View>
</View>
<View style={styles.listingtouch}>
<View style={styles.mainviewdetails}>
  <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
    <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
      <Text style={styles.listingmaintitledetails}>Amount</Text>
    </View>
    <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
      <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
        <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
      </View>
    </View>
  </View>
</View>
</View>
<View style={styles.listingtouch}>
<View style={styles.mainviewdetails}>

  <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
    <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
      <Text style={styles.listingmaintitledetails}>Quantity</Text>
    </View>
    <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
      <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
    </View>
  </View>
</View>
</View>
<View style={styles.listingtouch}>
<View style={styles.mainviewdetails}>
  <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
    <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
      <Text style={styles.listingmaintitledetails}>Amount Total</Text>
    </View>
    <View style={{ justifyContent: "flex-start", width: '50%', marginTop: -10, alignItems: "flex-start" }}>
      <TextInput style={{ height: 40, borderWidth: 1, borderColor: '#dddddd', backgroundColor: '#ffffff', width: 150 }} />
    </View>
  </View>
</View>
</View>

          <View style={{ flex: 1, padding: 4, height: '40%' }}>
            {this.state.valueArray.map(ele => {
              return (
                <Animated_Item
                  key={ele.id}
                  item={ele}
                  deleteItem={id => this.remove_Selected_Item(id)}
                  afterAnimationComplete={this.afterAnimationComplete}
                />
              );
            })}
          </View>

          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Total</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>24773.00</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', color: "#d26a5c", alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Verified by </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Lisa Jenkins</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <View style={styles.listingtouchgrey}>
          <View style={styles.mainviewdetails}>
            <View style={{ flexDirection: 'row', justifyContent: "center", alignItems: 'center' }} >
              <TouchableOpacity
                activeOpacity={0.8}
                disabled={this.state.disabled}
                onPress={this.add_New_View} style={{ justifyContent: "center", width: '27%', backgroundColor: "#f39c12", marginRight: 5, paddingTop: 12, paddingBottom: 5, borderRadius: 5, paddingLeft: 5, paddingRight: 5, alignItems: "center" }}>
                <Text style={styles.listingmaintitledetails} style={{ color: "#ffffff", fontSize: 16, fontWeight: "bold" }}>Add Row</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('Verifierinvoicelisting')} style={{ justifyContent: "center", width: '25%', backgroundColor: "#00a65a", marginRight: 5, paddingTop: 12, paddingBottom: 5, borderRadius: 5, paddingLeft: 5, paddingRight: 5, alignItems: "center" }}>
                <Text style={styles.listingmaintitledetails} style={{ color: "#ffffff", fontSize: 16, fontWeight: "bold" }}>Approve</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('Verifierinvoicelisting')} style={{ justifyContent: "center", width: '25%', backgroundColor: "#dd4b39", marginRight: 5, paddingTop: 12, paddingBottom: 5, borderRadius: 5, paddingLeft: 5, paddingRight: 5, alignItems: "center" }}>
                <Text style={styles.listingmaintitledetails} style={{ color: "#ffffff", fontSize: 16, fontWeight: "bold" }}>Reject</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eee',
    justifyContent: 'center',
    paddingTop: Platform.OS == 'ios' ? 20 : 0,
  },

  singleItemView: {
    //backgroundColor: '#FF6F00',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingVertical: 16,
    paddingLeft: 16,
    borderRadius: 8,
  },

  singleItemText: {
    color: '#d26a5c',
    fontSize: 18,
    fontWeight: 'bold',
    paddingRight: 18,
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 38,
    bottom: 38,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },

  deleteButton: {
    position: 'absolute',
    left: 320,
    width: 35,
    height: 25,
    padding: 7,
    justifyContent: 'center',
    alignItems: 'center',

  },

  removeIcon: {
    width: '100%',
    fontSize: 20,
    color: 'red',
  },

  listingtouch: {
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderColor: '#e6e6e6',
    alignItems: 'center',
  },

  listingmaintitledetails: {
    color: 'black',
    fontWeight: '500',
    fontSize: 15
  },


  mainviewdetails: {
    padding: 20,
    flexDirection: 'row',
    height: 60
  },

  listingtouchgrey: {
    backgroundColor: '#f9f9f9',
    borderBottomWidth: 1,
    borderColor: '#e6e6e6',
    alignItems: 'center',
    justifyContent:'center'
  },


});
