import React, { Component } from 'react';
import { Text, View, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity, BackHandler, ToastAndroid, Alert } from 'react-native';

import MenuButton from '../Component/MenuButton';
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';


export default class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      clicks: 0,
      show: true,
      branch: this.props.navigation.state.params.branch,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    //ToastAndroid.show('Exit Soon', ToastAndroid.SHORT);
    Alert.alert(
      'Exit App',
      'Are Your That You Want To Exit ?',
      [
        {
          text: 'Cancel',
          onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ]
    );
    return true;
  }


  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>

        <MenuButton navigation={this.props.navigation} />
        <ScrollView style={{ backgroundColor: '#ffffff' }}>
        {/* <Text style={{color:'black',fontWeight:'bold'}}>{this.state.branch}</Text> */}

          <View>
            <View style={styles.container}>

              <View style={styles.flexboxcode}>
                <TouchableOpacity onPress={() => navigate('Listing', { branchid: this.state.branch })} style={styles.box1}>
                  <FontAwesome name="user-o" size={38} color="#ffffff" />
                  <Text style={{ color: '#ffffff', fontWeight: 'bold', fontSize: 16 }}>Participants</Text>
                  <Text style={styles.boxtitle}>Registration</Text>

                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('Selectionsfee', { branchid: this.state.branch })} style={styles.box2}>
                  <FontAwesome5 name="money-bill-alt" size={38} color="#ffffff" />
                  <Text style={styles.boxtitle}>Fees</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>
              </View>

              <View style={styles.flexboxcode}>
                <TouchableOpacity onPress={() => navigate('Selectionsexam', { branchid: this.state.branch })} style={styles.box3}>
                  <FontAwesome5 name="marker" size={38} color="#ffffff" />
                  <Text style={styles.boxtitle}>Exams</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('Selections', { branchid: this.state.branch })} style={styles.box4}>
                  <FontAwesome name="users" size={38} color="#ffffff" />
                  <Text style={{ color: '#ffffff', fontWeight: 'bold', fontSize: 16 }}>Participants</Text>
                  <Text style={styles.boxtitle}>Attendance</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

              </View>

              <View style={styles.flexboxcode}>

                <TouchableOpacity style={styles.box6}>
                  <FontAwesome name="gear" size={38} color="#ffffff" />
                  <Text style={styles.boxtitle}>Settings</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>
              </View>
             

            </View>

          </View>

        </ScrollView>
      </SafeAreaView>
    );
  }
}
module.exports = HomeScreen;

const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%',
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef', justifyContent: 'center', alignItems: 'center'
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a', justifyContent: 'center', alignItems: 'center'
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12', justifyContent: 'center', alignItems: 'center'
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8', justifyContent: 'center', alignItems: 'center'
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4', justifyContent: 'center', alignItems: 'center'
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39', justifyContent: 'center', alignItems: 'center'
  },

})