import React, { Component } from 'react';
import { Text, View, AppRegistry, StatusBar, ScrollView, FlatList, StyleSheet, SafeAreaView, TouchableOpacity, TextInput, Header, Image } from 'react-native';
// import Icon from './node_modules/react-native-vector-icons/FontAwesome';
// import Icon1 from './node_modules/react-native-vector-icons/Entypo';
import { FontAwesome } from '@expo/vector-icons';



export default class Workflowlisting extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>
        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={25} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Manage Workflow</Text>
          </View>
          <TouchableOpacity onPress={() => navigate('Addworkflow')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="plus" size={20} color="#ffffff" />
          </TouchableOpacity>
        </View>
        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>
          <TouchableOpacity onPress={() => navigate('Addworkflow')} style={styles.listingtouch}>
            <View style={styles.mainviewlisting}>

              <View style={styles.textviewwidth} >
                <View style={styles.titlespacing}>
                  <Text style={styles.listingmaintitle}>Jesse Fisher  </Text>
                </View>
                <View>
                  <Text style={styles.approvestatus}>Type 1 User</Text>
                </View>
              </View>
              <View style={styles.listingpen}>
                <FontAwesome name="pencil" size={20} color="#555555" />
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  circlegray: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#cccccc', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  incativestatus: { color: '#dd4b39', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },
  listingpen: { width: '15%', justifyContent: "center", alignItems: "center",paddingLeft:35 },

})




