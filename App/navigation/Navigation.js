import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

import DrawerNavigator from './DrawerNavigator';
import Login from '../Login';
import HomeScreen from '../Screen/HomeScreen';

const Navigation = createDrawerNavigator(
    {
        Login: {
            screen: Login,
        },
        DrawerNavigator: {
            screen: DrawerNavigator,
        },
        Home: {
            screen: HomeScreen,
        },
    },
   
)

export default createAppContainer(Navigation);