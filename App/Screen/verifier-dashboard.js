import React, { Component } from 'react';
import { Text, View, AppRegistry, StatusBar, ScrollView, FlatList, StyleSheet, SafeAreaView, TouchableOpacity, TextInput, Header, Image } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import MenuButton from '../Component/MenuButton';

export default class Verifierdashboard extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>

        <MenuButton navigation={this.props.navigation} />

        <ScrollView style={{ backgroundColor: '#ffffff' }}>
          <View>

            <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#f5f5f5', borderBottomWidth: 1, borderBottomColor: '#e5e5e5', paddingTop: 10, paddingBottom: 20, paddingLeft: 8, paddingRight: 10 }}>

              <TouchableOpacity style={{ width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, justifyContent: "center", alignItems: "center", backgroundColor: '#00BCD4' }}>
                <FontAwesome name="upload" size={30} color="#ffffff" />
                <Text style={{ color: "#ffffff", fontWeight: "bold", fontSize: 16, marginBottom: 5, marginTop: 5 }}>Upload Invoice</Text>
                <Text style={{ color: "#ffffff", fontWeight: "500", fontSize: 14 }}>Only PDF invoices are allowed</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigate('Verifierinvoicelisting')} style={{ width: '49%', marginTop: 10, height: 150, padding: 10, justifyContent: "center", alignItems: "center", backgroundColor: '#00BCD4' }}>
                <FontAwesome name="address-book" size={30} color="#ffffff" />
                <Text style={{ color: "#ffffff", fontWeight: "bold", fontSize: 16, marginBottom: 5, marginTop: 5 }}>Verifier Invoice</Text>
                <Text style={{ color: "#ffffff", fontWeight: "500", fontSize: 14 }}>Invoice verification  and rejection</Text>
              </TouchableOpacity>
            </View>



            <View style={styles.container}>

              <View style={styles.flexboxcode}>

                <TouchableOpacity onPress={() => navigate('Verifierlisting')} style={styles.box1}>
                  <Text style={styles.boxnumber}>150</Text>
                  <Text style={styles.boxtitle}>Invoices Assigned</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('Verifierlisting')} style={styles.box2}>
                  <Text style={styles.boxnumber}>53</Text>
                  <Text style={styles.boxtitle}>Invoices pending for Data Extraction</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>
              </View>

              <View style={styles.flexboxcode}>
                <TouchableOpacity onPress={() => navigate('Verifierlisting')} style={styles.box3}>
                  <Text style={styles.boxnumber}>44</Text>
                  <Text style={styles.boxtitle}>Invoices pending for Verification</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('Verifierlisting')} style={styles.box4}>
                  <Text style={styles.boxnumber}>31</Text>
                  <Text style={styles.boxtitle}>Invoices pending for Approval</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

              </View>

              <View style={styles.flexboxcode}>
                <TouchableOpacity onPress={() => navigate('Verifierlisting')} style={styles.box5}>
                  <Text style={styles.boxnumber}>26</Text>
                  <Text style={styles.boxtitle}>Unpaid Invoices</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('Verifierlisting')} style={styles.box6}>
                  <Text style={styles.boxnumber}>65</Text>
                  <Text style={styles.boxtitle}>Overdue Invoices</Text>
                  <Text style={styles.boxviewall}>View All Data <FontAwesome name="chevron-right" size={10} color="#ffffff" /></Text>
                </TouchableOpacity>
              </View>

            </View>

          </View>

        </ScrollView>




      </SafeAreaView>


    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },

})



