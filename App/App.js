import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Splash from './Splash';

import DrawerNavigator from './navigation/DrawerNavigator';
//import addre from './addre';

const AppNavigator = createStackNavigator(
     {
          
          Splash: Splash,
          DrawerNavigator: DrawerNavigator,
          // addre:addre,

     },
     {
          headerMode:'none',
          initialRouteName: 'Splash'
     },
);
export default createAppContainer(AppNavigator);  
