import React from 'react';
import { StyleSheet,View,Text } from 'react-native';

import {Ionicons} from "@expo/vector-icons";


export default class MenuButton extends React.Component{
    render(){
        return(
            
            
          <View style={{height: '10%',flexDirection: 'row'}}>
          <View style={{width: '20%',justifyContent:'center',alignItems:'center' }} >
          <Ionicons
            name="md-menu"
            color="white"
            size={32}
            style={styles.menuIcon}
            onPress={() => this.props.navigation.toggleDrawer()}
          /> 
          </View>
          <View style={{width: '60%', justifyContent:'center',alignItems:'center', top:10,}} ><Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Dashboard</Text></View>        
           <View style={{width: '20%', }} />
        </View>

        )
    }
}

const styles = StyleSheet.create({
    menuIcon:{
        zIndex:9,
        position:'absolute',
        top:30,
        left:20,
    },
    mainView:{
        flex: 1,
        flexDirection: 'column',        
        alignItems: 'stretch',
    },
   
})

