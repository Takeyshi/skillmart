import React, { Component } from 'react';
import { Text, View, AppRegistry, StatusBar, ScrollView, Alert, StyleSheet, SafeAreaView, Button, TouchableOpacity, TextInput, Header, Image } from 'react-native';
// import Icon from './node_modules/react-native-vector-icons/FontAwesome';
// import Icon1 from './node_modules/react-native-vector-icons/Entypo';
import { FontAwesome } from '@expo/vector-icons';



export default class Verifierdetails extends Component {
  render() {
    const { navigate } = this.props.navigation;

    return ( 
     
      


<SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>

        
          <View style={{height: '12%', flexDirection: 'row', backgroundColor: '#0186C2'}} >
              <TouchableOpacity onPress={() =>navigate('Verifierlisting')} style={{height: '100%', width: '15%', marginTop:10, justifyContent: "center", alignItems: "center"}}>
                  <FontAwesome name="angle-left" size={30} color="#ffffff" />
              </TouchableOpacity>
              <View style={{height: '100%', width: '65%', marginTop:10, justifyContent: "center", alignItems: "center"}} >
                  <Text style={{color:"#ffffff", fontSize:18, fontWeight:"bold"}}>Verifier Details</Text>
              </View>
              <TouchableOpacity onPress={() =>navigate('Editverifierdetails')} style={{height: '100%', width: '8%', marginTop:10, justifyContent: "center", alignItems: "center"}}>
                  <FontAwesome name="edit" size={25} color="#ffffff" />
              </TouchableOpacity>
              <TouchableOpacity onPress={() =>navigate('Extracteddataverifier')} style={{height: '100%', width: '10%', marginTop:10, justifyContent: "center", alignItems: "center"}}>
                  <FontAwesome name="eye" size={25} color="#ffffff" />
              </TouchableOpacity>
          </View>

          
          <ScrollView style={{backgroundColor: '#ffffff',  paddingBottom: '3%'}}>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Invoice ID </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>#829 </Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Type 1 User </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>Lisa Jenkins</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Assigned Date </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>29/05/2019</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Data Extraction Date</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>29/05/2019</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Verified Date </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>29/05/2019</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Approved Date </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>29/05/2019</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Processed Date </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>29/05/2019</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Due Date </Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>29/05/2019</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Priority</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainttext}>High</Text>
                </View>
              </View>
              </View>
              </View>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Status</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainpriority}>Approved</Text>
                </View>
              </View>
              </View>
              </View>


              <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Invoice PDF</Text>
                </View>
                <TouchableOpacity onPress={() => {
                  Alert.alert(
                    'PDF View',
                    'Would you like to open PDF??',
                    [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      { text: 'OK', onPress: () => navigate("pdfview") },
                    ]
                  );
                }}
                  style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <FontAwesome name="file" size={20} color="#000000" />
                </TouchableOpacity>
              </View>
            </View>
          </View>


            <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>
              
              <View style={{flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start"}} >
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmaintitledetails}>Priority</Text>
                </View>
                <View style={{justifyContent: "flex-start", width:'50%', alignItems: "flex-start"}}>
                  <Text style={styles.listingmainpriority}>High</Text>
                </View>
              </View>
              </View>
              </View>


           </ScrollView>


     

</SafeAreaView>
        
        
    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,     
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
  color:"#ffffff", fontWeight:"bold", fontSize:30, marginBottom:5,
  },

  boxtitle: {
  color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
  color:"#ffffff", fontWeight:"500", fontSize:12,
  },
  container: {
  paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
  flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop:10, marginRight:10, padding:10, height: 150, paddingTop:20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop:10, marginRight:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%',  marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop:10, marginRight:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%',  marginTop:10, height: 150, padding:10, paddingTop:20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40/2, marginRight:10,
    backgroundColor: '#dd4b39',alignItems:'center', justifyContent:'center',
},
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40/2, marginRight:10,
    backgroundColor: '#00a65a',alignItems:'center', justifyContent:'center',
},
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40/2, marginRight:10,
    backgroundColor: '#f39c12',alignItems:'center', justifyContent:'center',
},
  listingtouch: {
    backgroundColor:'#ffffff', borderBottomWidth:1, borderColor:'#e6e6e6', alignItems:'center',
  },
  circletitle: {color:'white',fontWeight:'bold',fontSize:20},
  listingmaintitle: {color:'black',fontWeight:'500',fontSize:16},
  approvestatus: {color:'#00a65a', fontSize:13},
  listingrightarrow: {width: '15%', justifyContent: "center", alignItems: "center"},
  mainviewlisting: {padding:20,flexDirection:'row', height:80},
  textviewwidth: {width: '75%'},
  titlespacing: {marginBottom:5},


  mainviewdetails: {padding:20,flexDirection:'row', height:60},

  listingmainpriority: {color:'green',fontWeight:'500',fontSize:16},

  listingtouchgrey: {
    backgroundColor:'#f9f9f9', borderBottomWidth:1, borderColor:'#e6e6e6', alignItems:'center',
  },

  listingmaintitledetails: {color:'black',fontWeight:'500',fontSize:15},
  listingmainttext: {color:'black',fontWeight:'400',fontSize:15},

})




