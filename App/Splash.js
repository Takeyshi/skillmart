
import React, { Component } from 'react';

import { StyleSheet, View, Image} from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';


 
export default class FirstPage extends Component {
   
  static navigationOptions = {
    header: null
}
        componentWillMount() {
          setTimeout(() => {
              this.props.navigation.navigate('DrawerNavigator')
          }, 4000);
      }
 
  render() {
   
    return (
      <View style={styles.container}>
      <LinearGradient colors={['#ffffff','#ffffff','#ffffff']} style={styles.linearGradient} >
      <View style={{flex:1,justifyContent: 'center',alignItems: 'center',}}>
     <Image 
        style={{height:'15%',width:'88%',}}
        source={require('./image/Welcome.png')}
     />
     
      </View>
      </LinearGradient>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: 
  {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  
  },
  linearGradient: 
  {
    flex: 1,
    width:'100%',
    height:'100%',
   },
  logo:
   {
    height: 70,
     width:350,  
   },
  
});