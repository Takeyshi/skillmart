import React, { Component } from 'react';
import { Text, View, AppRegistry, Alert, Picker, ScrollView, FlatList, StyleSheet, SafeAreaView, Button, TouchableOpacity, TextInput, Header, Image } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default class Selections extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      branch: [],
      batch: [],


    };
  }

  loaddata() {
    fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/option', {
      method: 'GET'
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          branch: responseJson.branch,
          batch: responseJson.batch,
          data: responseJson.courses
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  componentDidMount() {
    this.loaddata()
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({
        language1: 'Select Batch',
        language2: 'Select Course'
      })

    });
  }
  componentDidUpdate() { this.loaddata() }

  Send_Data_Function(branch1, batch1, course1) {
    
    //console.log("SELECT",branch1 + batch1 + course1)
    if (branch1 != '') {
      if (batch1 != '') {
        if (course1 != '') {
          this.props.navigation.navigate('Attendancelist', {
            branch1: branch1,
            batch1: batch1,
            course1: course1,
          });
        }
        else { alert("Please select all filde first.") }
      }
      else { alert("Please select all filde first.") }
    }
    else { alert("Please select all filde first.") }


  }

  render() {
    const { navigate } = this.props.navigation;
    return (

      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>

        {/*console.log("branch" + this.state.language + "batch" + this.state.language1 + "course" + this.state.language2)*/}
        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Home')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={30} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Select  Your Options</Text>
          </View>
          <TouchableOpacity onPress={() => this.Send_Data_Function(this.props.navigation.state.params.branchid, this.state.language1, this.state.language2)}
            style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="save" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>


        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>        


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Branch</Text>
                </View>
                <View style={{ justifyContent: "center", width: '50%', marginTop: -10, }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: 170, fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.props.navigation.state.params.branchid}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language: itemValue, choosenIndex: itemPosition })} >


                    {this.state.branch.map((item, key) => (
                      <Picker.Item label={item.branch_name} value={item.branch_id} key={key} />)
                    )}

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Batch</Text>
                </View>
                <View style={{ justifyContent: "center", width: '50%', marginTop: -10, }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: 170, fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language1}

                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language1: itemValue, choosenIndex1: itemPosition })}
                  >
                    <Picker.Item label="Select Batch" value="" />
                    {this.state.batch.map((item, key) => (
                      <Picker.Item label={item.batchname} value={item.batch_id} key={key} />)
                    )}

                  </Picker>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Course</Text>
                </View>
                <View style={{ justifyContent: "center", width: '50%', marginTop: -10, }}>
                  <Picker style={{
                    justifyContent: 'center', alignItems: 'center', height: 40,
                    borderWidth: 1, borderColor: '#dddddd', backgroundColor: this.state.buttonColor,
                    width: 170, fontWeight: 'bold', paddingLeft: 5
                  }}
                    selectedValue={this.state.language2}
                    onValueChange={(itemValue, itemPosition) =>
                      this.setState({ language2: itemValue, choosenIndex2: itemPosition })}
                  >
                    <Picker.Item label="Select Course" value="" />
                    {this.state.data.map((item, key) => (
                      <Picker.Item label={item.course_name} value={item.course_id} key={key} />)
                    )}
                  </Picker>
                </View>
              </View>
            </View>
          </View>


        </ScrollView>
      </SafeAreaView>


    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },


  mainviewdetails: { padding: 20, flexDirection: 'row', height: 60 },

  listingmainpriority: { color: 'green', fontWeight: '500', fontSize: 16 },

  listingtouchgrey: {
    backgroundColor: '#f9f9f9', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },

  listingmaintitledetails: { color: 'black', fontWeight: '500', fontSize: 15 },
  listingmainttext: { color: 'black', fontWeight: '400', fontSize: 15 },

})




