import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, SafeAreaView, TouchableOpacity, Alert, } from 'react-native';
// import Icon from './node_modules/react-native-vector-icons/FontAwesome';
// import Icon1 from './node_modules/react-native-vector-icons/Entypo';
import { FontAwesome } from '@expo/vector-icons';


export default class Details extends Component {

  Send_Data_Function(id, name, mobile, alter, aadhar, courseno, Email, branchid) {
console.log("Add detail",id, name, mobile, alter, aadhar, courseno, Email, branchid)
    this.props.navigation.navigate('Adddetails', {
      id: id,
      name: name,
      number: mobile,
      alter: alter,
      aadhar: aadhar,
      courseno: courseno,
      Email: Email,
      branchid: branchid,
    });

  }

  render() {
    const { navigate } = this.props.navigation;



    return (
      <SafeAreaView style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch', backgroundColor: '#0186C2',
      }}>


        <View style={{ height: '12%', flexDirection: 'row', backgroundColor: '#0186C2' }} >
          <TouchableOpacity onPress={() => navigate('Listing')} style={{ height: '100%', width: '15%', marginTop: 10, justifyContent: "center", alignItems: "center" }}>
            <FontAwesome name="angle-left" size={30} color="#ffffff" />
          </TouchableOpacity>
          <View style={{ height: '100%', width: '70%', marginTop: 10, justifyContent: "center", alignItems: "center" }} >
            <Text style={{ color: "#ffffff", fontSize: 18, fontWeight: "bold" }}>Details</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.Send_Data_Function(this.props.navigation.state.params.id, this.props.navigation.state.params.name, this.props.navigation.state.params.number
              , this.props.navigation.state.params.alter, this.props.navigation.state.params.aadhar, this.props.navigation.state.params.courseno, this.props.navigation.state.params.Email, this.props.navigation.state.params.branchid)} >
            <FontAwesome name="edit" size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>


        <ScrollView style={{ backgroundColor: '#ffffff', paddingBottom: '3%' }}>
          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Participant ID </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>{this.props.navigation.state.params.id}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Participant Name </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.name}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Mobile Number </Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.number}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Alternate Phone</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.alter}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Aadhar Number</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.aadhar}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouchgrey}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Course Name</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.courseno}</Text>
                </View>
              </View>
            </View>
          </View>


          <View style={styles.listingtouch}>
            <View style={styles.mainviewdetails}>

              <View style={{ flexDirection: 'row', justifyContent: "flex-start", alignItems: "flex-start" }} >
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmaintitledetails}>Email Address</Text>
                </View>
                <View style={{ justifyContent: "flex-start", width: '50%', alignItems: "flex-start" }}>
                  <Text style={styles.listingmainttext}>{this.props.navigation.state.params.Email}</Text>
                </View>
              </View>
            </View>
          </View>

        </ScrollView>




      </SafeAreaView>


    );
  }
}


const styles = StyleSheet.create({

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderColor: '#000',
    borderBottomWidth: 2,
    backgroundColor: '#999',
    marginBottom: 10,
  },

  boxnumber: {
    color: "#ffffff", fontWeight: "bold", fontSize: 30, marginBottom: 5,
  },

  boxtitle: {
    color: '#ffffff', fontWeight: 'bold', fontSize: 16, marginBottom: 10,
  },

  boxviewall: {
    color: "#ffffff", fontWeight: "500", fontSize: 12,
  },
  container: {
    paddingTop: '3%', paddingLeft: '2%', paddingRight: '3%', paddingBottom: '3%'
  },

  flexboxcode: {
    flex: 1, flexDirection: 'row',
  },

  box1: {
    width: '49%', marginTop: 10, marginRight: 10, padding: 10, height: 150, paddingTop: 20, backgroundColor: '#00c0ef',
  },

  box2: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#00a65a',
  },

  box3: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#f39c12',
  },

  box4: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#018dc8',
  },

  box5: {
    width: '49%', marginTop: 10, marginRight: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#a68ad4',
  },

  box6: {
    width: '49%', marginTop: 10, height: 150, padding: 10, paddingTop: 20, backgroundColor: '#dd4b39',
  },
  circlered: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#dd4b39', alignItems: 'center', justifyContent: 'center',
  },
  circlegreen: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#00a65a', alignItems: 'center', justifyContent: 'center',
  },
  circleyellow: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2, marginRight: 10,
    backgroundColor: '#f39c12', alignItems: 'center', justifyContent: 'center',
  },
  listingtouch: {
    backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },
  circletitle: { color: 'white', fontWeight: 'bold', fontSize: 20 },
  listingmaintitle: { color: 'black', fontWeight: '500', fontSize: 16 },
  approvestatus: { color: '#00a65a', fontSize: 13 },
  listingrightarrow: { width: '15%', justifyContent: "center", alignItems: "center" },
  mainviewlisting: { padding: 20, flexDirection: 'row', height: 80 },
  textviewwidth: { width: '75%' },
  titlespacing: { marginBottom: 5 },


  mainviewdetails: { padding: 20, flexDirection: 'row', height: 60 },

  listingmainpriority: { color: 'green', fontWeight: '500', fontSize: 16 },

  listingtouchgrey: {
    backgroundColor: '#f9f9f9', borderBottomWidth: 1, borderColor: '#e6e6e6', alignItems: 'center',
  },

  listingmaintitledetails: { color: 'black', fontWeight: '500', fontSize: 15 },
  listingmainttext: { color: 'black', fontWeight: '400', fontSize: 15 },

})




