import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BackButton from '../Component/BackButton';

import { LinearGradient } from 'expo-linear-gradient';

export default class SettingScreen extends Component {
  render() {
    return (
      <LinearGradient colors={['#018ec5','#155B7D','#124C69']} style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <BackButton navigation={this.props.navigation}/>
        <Text>Hello, SettingScreen!</Text>
      </LinearGradient>
    );
  }
}
