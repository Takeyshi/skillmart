import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';

export default class Splash extends Component {


  static navigationOptions = {
        header: null
    }

  
   componentDidMount() {

        setTimeout(() => {
            this.props.navigation.navigate('Test')
        }, 2500)
    }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", backgroundColor:'#0189c3', alignItems: "center" }}>
         <Image source={require('../images/logo.png')} style={{resizeMode: 'stretch', width:200, height:60}} />

      </View>
    );
  }
}
